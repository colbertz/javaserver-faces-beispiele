package de.christopherolbertz.spacegame.service.interfaces;

import java.util.List;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.User;

public interface PlanetService {
	public void delete(Planet planet);
	public void delete(int planetId);
	public Planet findPlanet(long id);
	public List<Planet> getAllPlanets();
	public void savePlanet(Planet planet);
	public void reduceMetalStorage(int amount, Planet planet);
	public void reduceCrystalStorage(int amount, Planet planet);
	public List<Planet> findPlanetsByUser(User user);
	/**
	 * Startet einen Angriff auf einen Planeten. 
	 * @param targetPlanet Der feindliche Planet, der angegriffen wird.
	 * @param attackingStarships Die angreifenden Raumschiffe.
	 * @return True, wenn der Angriff erfolgreich war, sonst false.
	 */
	boolean attackPlanet(Planet targetPlanet, List<Starship> attackingStarships);
	Planet findPlanetByName(String planetName);
}
