package de.christopherolbertz.spacegame.utils;

public class StringUtils {
	public static boolean isNotEmpty(final String aString) {
		if (aString == null) {
			return false;
		}
		return !aString.trim().isEmpty();
	}

	public static boolean isEmpty(final String aString) {
		if (aString == null) {
			return true;
		}
		return aString.trim().isEmpty();
	}
	
	public static boolean areStringsEqual(final String string1, final String string2) {
		if (string1.equals(string2)) {
			return true;
		} else {
			return false;
		}
	}
}
