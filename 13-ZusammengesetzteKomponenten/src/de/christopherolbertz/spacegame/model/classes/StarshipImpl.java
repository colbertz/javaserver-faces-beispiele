package de.christopherolbertz.spacegame.model.classes;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;

public abstract class StarshipImpl implements Starship {
	private long starshipId;
	private String starshipName;
	private double damage;
	private Planet currentPlanet;
	private boolean active;
	private static final int MAX_DAMAGE = 100;
	private static final int MIN_DAMAGE = 0;

	public StarshipImpl(String starshipName, Planet currentPlanet) {
		super();
		this.starshipName = starshipName;
		this.currentPlanet = currentPlanet;
	}

	@Override
	public Planet getCurrentPlanet() {
		return currentPlanet;
	}

	@Override
	public void setCurrentPlanet(Planet currentPlanet) {
		this.currentPlanet = currentPlanet;
	}
	
	public StarshipImpl(String starshipName, double damage) {
		this.starshipName = starshipName;
		this.damage = damage;
	}
	
	public StarshipImpl(long starshipId, String name, Planet currentPlanet) {
		this(name,currentPlanet);
		this.starshipId = starshipId;
	}

	@Override
	public long getStarshipId() {
		return starshipId;
	}

	@Override
	public void setStarshipId(long starshipId) {
		this.starshipId = starshipId;
	}

	@Override
	public String getStarshipName() {
		return starshipName;
	}

	@Override
	public void setStarshipName(String starshipName) {
		this.starshipName = starshipName;
	}

	@Override
	public double getDamage() {
		return damage;
	}

	@Override
	public void setDamage(double damage) {
		this.damage = damage;
	}

	@Override
	public String toString() {
		return String.valueOf(starshipId) + ": " + starshipName;
	}
	
	@Override
	public void destroy() {
		damage = MAX_DAMAGE;
	}
	
	@Override
	public boolean isActive() {
		return active;
	}
	
	@Override
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Override
	public void damage(final double attackStrength, final double defendStrength) {
		final double damageValue = defendStrength - attackStrength;
		if (damageValue < 0) {
			damage = damage + attackStrength;
		} else {
			damage = damage + damageValue;
		}
		
		if (damageValue > MAX_DAMAGE) {
			destroy();
		}
	}
	
	@Override
	public boolean isDestroyed() {
		if (damage >= MAX_DAMAGE) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isNotDestroyed() {
		if (damage < MAX_DAMAGE) {
			return true;
		} else {
			return false;
		}
	}
	
	//public abstract boolean isFrigate();	
	@Override
	public abstract int getCrystalCost();
	@Override
	public abstract int getMetalCost();
}
