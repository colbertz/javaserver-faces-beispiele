package de.christopherolbertz.spacegame.service.classes;

import java.util.List;

import de.christopherolbertz.spacegame.dao.classes.PlanetDaoImpl;
import de.christopherolbertz.spacegame.dao.classes.UserDaoImpl;
import de.christopherolbertz.spacegame.dao.interfaces.PlanetDao;
import de.christopherolbertz.spacegame.dao.interfaces.UserDao;
import de.christopherolbertz.spacegame.factories.ModelFactory;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.interfaces.UserService;

public class UserServiceImpl implements UserService {
	private PlanetDao planetDao;
	private UserDao userDao;
	
	public UserServiceImpl() {
		userDao = UserDaoImpl.getInstance();
		planetDao = PlanetDaoImpl.getInstance();
	}
	
	@Override
	public void delete(final User user){
		userDao.delete(user);
	}
	
	@Override
	public User findUser(final long userId) {
		return userDao.findUser(userId);
	}
	
	@Override
	public List<User> findAllUsers() {
		return userDao.getList();
	}
	
	@Override
	public User findUserByName(final String userName) {
		return userDao.findUserByName(userName);
	}
	
	@Override
	public void saveUser(final User user) {
		if (user.getUserId() == 0) {
			Planet planet = ModelFactory.createPlanetWithRandomName();
			user.getMyPlanets().add(planet);
			planetDao.savePlanet(planet);
		}
		userDao.saveUser(user);
	}
	
	@Override
	public int countUsers() {
		return userDao.countUsers();
	}
}