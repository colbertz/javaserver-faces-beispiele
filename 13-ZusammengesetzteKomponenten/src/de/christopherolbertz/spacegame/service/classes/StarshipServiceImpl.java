package de.christopherolbertz.spacegame.service.classes;

import java.util.ArrayList;
import java.util.List;

import de.christopherolbertz.spacegame.dao.classes.StarshipDaoImpl;
import de.christopherolbertz.spacegame.dao.interfaces.StarshipDao;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;
import de.christopherolbertz.spacegame.utils.RandomUtils;

public class StarshipServiceImpl implements StarshipService {
	private StarshipDao starshipDao;
	
	public StarshipServiceImpl() {
		starshipDao = StarshipDaoImpl.getInstance();
	}
	
	@Override
	public void deleteStarship(final Starship starship){
		starshipDao.delete(starship);
	}
	
	@Override
	public void deleteStarship(final int starshipId){
		final Starship starship = findStarship(starshipId);
		starshipDao.delete(starship);
	}
	
	@Override
	public Starship findStarship(final long starshipIdd) {
		return starshipDao.findStarship(starshipIdd);
	}
	
	@Override
	public List<Starship> findAllStarships() {
		return starshipDao.findAllStarships();
	}
	
	@Override
	public List<Starship> findAllStarshipsOnPlanet(final Planet planet) {
		return starshipDao.findAllStarshipsOnPlanet(planet);
	}
	
	@Override
	public List<Starship> findStarshipsByIds(final List<String> starshipIds) {
		final List<Starship> starships = new ArrayList<>();
		
		for (String starshipId: starshipIds) {
			Long id = Long.valueOf(starshipId);
			starships.add(starshipDao.findStarship(id));
		}
		
		return starships;
	}
	
	@Override
	public void stationStarshipOnPlanet(final Starship starship, final Planet planet) {
		planet.getStarshipsOnPlanet().add(starship);
	}
	
	@Override
	public void saveStarship(final Starship starship) {
		if (starship.getStarshipId() == 0) {
			starship.setStarshipId(RandomUtils.createRandomStarshipId());
		}
		starshipDao.saveStarship(starship);
	}
	
	@Override
	public void saveStarships(final List<Starship> starshipList) {
		for (final Starship starship: starshipList) {
			saveStarship(starship);
		}
	}

	@Override
	public int countStarships() {
		return starshipDao.countStarships();
	}
	
	/*public int getMetalCostTransportShip() {
		return new TransportShipHibernateImpl().getMetalCost();
	}
	
	public int getCrystalCostTransportShip() {
		return new TransportShipHibernateImpl().getCrystalCost();
	}
	
	public int getMetalCostSpyShip() {
		return new SpyShipHibernateImpl().getMetalCost();
	}
	
	public int getCrystalCostSpyShip() {
		return new SpyShipHibernateImpl().getCrystalCost();
	}
	
	public int getMetalCostFrigate() {
		return new FrigateHibernateImpl().getMetalCost();
	}
	
	public int getCrystalCostFrigate() {
		return new FrigateHibernateImpl().getCrystalCost();
	}
	
	public boolean isStarshipNameAssigend(String starshipName, User user) {
		return starshipDao.isStarshipNameAssigend(starshipName, user);
	}*/
}
