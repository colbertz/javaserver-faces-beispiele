package de.christopherolbertz.spacegame.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.christopherolbertz.spacegame.i18n.I18nMessageUtil;
import de.christopherolbertz.spacegame.utils.JsfUtils;

@FacesValidator("passwordConfirmationValidator")
public class PasswordConfirmationValidator implements Validator {

	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String passwordConfirmation = (String)value;
		String password = (String)JsfUtils.getBeanAttribute
					("password", "registrationView", 
					  String.class);
		
		if (!passwordConfirmation.equals(password)) {
			String message = I18nMessageUtil.getPasswordsNotEqualString();
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
		}
	}
}
