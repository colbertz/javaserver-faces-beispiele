package de.christopherolbertz.spacegame.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;
import de.christopherolbertz.spacegame.utils.StringUtils;

@FacesConverter("starshipConverter")
public class StarshipConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		final StarshipService starshipService = ServiceFactory.createStarshipService();
		final String starshipIdAsString = StringUtils.everythingBeforeCharacter(':', value);
		final long starshipId = Long.parseLong(starshipIdAsString);
		final Starship starship = starshipService.findStarship(starshipId);
		return starship;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return value.toString();
	}

}
