package de.christopherolbertz.spacegame.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public class DualListHandler {
	/**
	 * Liste mit den verfügbaren und nicht ausgewählten Elementen.
	 */
	private List<SelectItem> availableItems;
	/**
	 * Das in der Liste mit den ausgewählten Elemente markierte Element. Es handelt sich somit um das Element, das
	 * aus der Auswahl herausgenommen werden soll.
	 */
	private SelectItem deselectedItem;
	/**
	 * True, wenn ein Datensatz in der rechten Liste ausgewählt wurde und die Schaltflächen mit dem Pfeil nach links somit aktiv sind.
	 */
	private boolean deselectionPossible; 
	/**
	 * Das in der Liste der verfügbaren Elemente ausgewählte Element.
	 */             
	private String selectedItem;
	/**
	 * Liste mit den ausgewählten Elementen.
	 */
	private List<SelectItem> selectedItems;
	/**
	 * True, wenn ein Datensatz in der linken Liste ausgewählt wurde und die Schaltflächen mit dem Pfeil nach rechts somit aktiv sind.
	 */
	private boolean selectionPossible; 
	
	// ******************************** Initialisierung ****************************************
	/**
	 * Holt sich das Argument availableItems aus der DualList-Komponente.
	 */
	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ELContext elContext = facesContext.getELContext();
		availableItems = (ArrayList<SelectItem>)facesContext.
				getApplication().getExpressionFactory().
				createValueExpression(elContext, 
						"#{availableItems.items}", 
						ArrayList.class).getValue(elContext);
		selectedItems = new ArrayList<>();
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
	}
	// ******************************** Action-Listener **************************************
	public void valueChange(ValueChangeEvent event) {
		System.out.println("*****************+");
	}
	
	/**
	 * Verschiebt alle ausgewählten Elemente in die Liste mit den verfügbaraen Elementen.
	 * @param event ActionEvent.
	 */
	public void deselectAllItems(ActionEvent event) {
		availableItems.addAll(selectedItems);
		selectedItems.clear();
	}
	
	/**
	 * Verschiebt ein Element aus der Liste der ausgewählten Elemente in die Liste der verfügbaren Elemente.
	 */
	public void deselectOneItem(ActionEvent event) {
		boolean found = false;
		int i = 0;
		
		// In der Liste nach dem ausgewählten Element suchen.
		while (found == false) {
			String myItem = selectedItems.get(i).getValue().toString();
			// Die SelectItem-Repräsentation des Wertes des aktuellen Elements ist gleich dem ausgewählten Element.
			if (myItem.equals(selectedItem)) {     
				// Element aus der linken Liste löschen.
				selectedItems.remove(selectedItems.get(i));
				// Element in die rechte Liste einfügen.
				if (availableItems == null) {
					availableItems = new ArrayList<SelectItem>();
				}
				availableItems.add(selectedItems.get(i));
				found = true;
			}
			i++;
		}
	}
	
	/**
	 * Verschiebt alle verfügbaren Elemente in die Liste mit den ausgewählten Elementen.
	 * @param event ActionEvent.
	 */
	public void selectAllItems(ActionEvent event) {
		selectedItems.addAll(availableItems);
		availableItems.clear();
	}
	
	/**
	 * Verschiebt ein Element aus der Liste der verfügbaren Elemente in die Liste der ausgewählten Elemente.
	 */
	public void selectOneItem(ActionEvent event) {
		boolean found = false;
		int i = 0;
		
		// In der Liste nach dem ausgewählten Element suchen.
		while (found == false) {
			String myItem = availableItems.get(i).getValue().toString();
			// Die SelectItem-Repräsentation des Wertes des aktuellen Elements ist gleich dem ausgewählten Element.
			if (myItem.equals(selectedItem)) {     
				SelectItem selected = availableItems.get(i);
				// Element aus der linken Liste löschen.
				availableItems.remove(selected);
				// Element in die rechte Liste einfügen.
				if (selectedItems == null) {
					selectedItems = new ArrayList<SelectItem>();
				}
				selectedItems.add(selected);
				found = true;
			}
			i++;
		}
	}
			
	// ******************************** Getter und Setter ************************************
	public String getSelectedItem() {
		return selectedItem;
	}
	
	public void setSelectedItem(String selectedItem) {
		this.selectedItem = selectedItem;
	}

	public SelectItem getDeselectedItem() {
		return deselectedItem;
	}

	public void setDeselectedItem(SelectItem deselectedItem) {
		this.deselectedItem = deselectedItem;
	}

	public boolean isDeselectionPossible() {
		return deselectionPossible;
	}

	public void setDeselectionPossible(boolean deselectionPossible) {
		this.deselectionPossible = deselectionPossible;
	}

	public boolean isSelectionPossible() {
		return selectionPossible;
	}

	public void setSelectionPossible(boolean selectionPossible) {
		this.selectionPossible = selectionPossible;
	}

	public List<SelectItem> getAvailableItems() {
		return availableItems;
	}

	public void setAvailableItems(List<SelectItem> availableItems) {
		this.availableItems = availableItems;
	}

	public List<SelectItem> getSelectedItems() {
		return selectedItems;
	}

	public void setSelectedItems(List<SelectItem> selectedItems) {
		this.selectedItems = selectedItems;
	}
}