package de.christopherolbertz.compositecomponents.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@NoneScoped
public class AvailableItems {
	private List<SelectItem> items;

	@PostConstruct
	public void init() {
		items = new ArrayList<>();
		items.add(new SelectItem("Apfel"));
		items.add(new SelectItem("Birne"));
		items.add(new SelectItem("Clementine"));
		items.add(new SelectItem("Dattel"));
		items.add(new SelectItem("Erdbeere"));
		items.add(new SelectItem("Feige"));
		items.add(new SelectItem("Granatapfel"));
		items.add(new SelectItem("Himbeere"));
	}
	
	public List<SelectItem> getItems() {
		return items;
	}

	public void setItems(List<SelectItem> items) {
		this.items = items;
	}
}
