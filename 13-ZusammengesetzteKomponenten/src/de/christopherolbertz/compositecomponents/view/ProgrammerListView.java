package de.christopherolbertz.compositecomponents.view;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ComponentSystemEvent;

@ManagedBean
public class ProgrammerListView {
	private List<Programmer> programmerList;
	
	public void initialize(ComponentSystemEvent event) {
		programmerList = new ArrayList<Programmer>();
		programmerList.add(new Programmer("Kent", "Beck"));
		programmerList.add(new Programmer("Erich", "Gamms"));
		programmerList.add(new Programmer("Ada", "Lovelace"));
		programmerList.add(new Programmer("Grace", "Hopper"));
		programmerList.add(new Programmer("Martin", "Fowler"));
		programmerList.add(new Programmer("Linus", "Thorvalds"));
	}

	public List<Programmer> getProgrammerList() {
		return programmerList;
	}

	public void setProgrammerList(List<Programmer> programmerList) {
		this.programmerList = programmerList;
	}
}