package de.christopherolbertz.compositecomponents.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Registration {
	private LocalDate registrationDate;
	private LocalTime registrationTime;
	
	public LocalDate getRegistrationDate() {
		return registrationDate;
	}
	
	public void setRegistrationDate(LocalDate registrationDate) {
		this.registrationDate = registrationDate;
	}
	
	public LocalTime getRegistrationTime() {
		return registrationTime;
	}
	
	public void setRegistrationTime(LocalTime registrationTime) {
		this.registrationTime = registrationTime;
	}
}
