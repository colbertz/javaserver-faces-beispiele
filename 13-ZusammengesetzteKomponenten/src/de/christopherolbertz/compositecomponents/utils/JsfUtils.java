package de.christopherolbertz.compositecomponents.utils;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;

public class JsfUtils {
	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	public static ELContext getELContext() {
		return FacesContext.getCurrentInstance().getELContext();
	}
	
	public static ExpressionFactory getExpressionFactory(FacesContext facesContext) {
		return getApplication(facesContext).getExpressionFactory();
	}
	  
	public static Application getApplication(FacesContext facesContext) {
		Application application = facesContext.getApplication(); 
		return application;
	}
}
