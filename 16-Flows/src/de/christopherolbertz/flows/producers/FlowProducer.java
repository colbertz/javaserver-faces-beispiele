package de.christopherolbertz.flows.producers;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.faces.flow.Flow;
import javax.faces.flow.builder.FlowBuilder;
import javax.faces.flow.builder.FlowBuilderParameter;
import javax.faces.flow.builder.FlowDefinition;

public class FlowProducer implements Serializable {
	@Produces
	@FlowDefinition
	public Flow buildRegistrationFlow(@FlowBuilderParameter FlowBuilder flowBuilder) {
		flowBuilder.id("", "registration");
		flowBuilder.viewNode("personalData", "/registration/personalData.xhtml")
			.markAsStartNode();
		flowBuilder.viewNode("addressData", "/registration/addressData.xhtml");
		flowBuilder.viewNode("confirmation", "/registration/confirmation.xhtml");
		flowBuilder.returnNode("finishRegistration").fromOutcome("/index.xhtml");
		flowBuilder.flowCallNode("callConfirmationFlow").flowReference("", "confirmation")
			.outboundParameter("email", "#{registrationView.emailAddress}");
		flowBuilder.initializer("#{registrationView.initFlowWithJava}");
		flowBuilder.finalizer("#{registrationView.finalizeFlowWithJava}");
		return flowBuilder.getFlow();
	}
	
	@Produces
	@FlowDefinition
	public Flow buildEmailConfirmationFlow(@FlowBuilderParameter FlowBuilder flowBuilder) {
		flowBuilder.id("", "confirmation");
		flowBuilder.viewNode("emailConfirmation", "/confirmation/emailConfirmation.xhtml")
			.markAsStartNode();
		flowBuilder.viewNode("confirmationSuccessful", "/confirmation/confirmationSuccessful.xhtml");
		flowBuilder.returnNode("finishConfirmation").fromOutcome("/index.xhtml");
		flowBuilder.inboundParameter("email", "#{flowScope.email}");
		
		return flowBuilder.getFlow();
	}
}
