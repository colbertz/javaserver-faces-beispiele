package de.christopherolbertz.flows.view;

import java.io.Serializable;

import javax.faces.flow.FlowScoped;
import javax.inject.Named;

@Named
@FlowScoped("registration")
public class RegistrationView implements Serializable {
	private String firstName;
	private String lastName;
	private String street;
	private String zipCode;
	private String city;
	private String emailAddress;
	
	public void initFlowWithJava() {
		System.out.println("Initialisiere Flow mit Java");
	}
	
	public void finalizeFlowWithJava() {
		System.out.println("Beende Flow mit Java");
	}
	
	public void initFlow() {
		System.out.println("Initialisiere Flow");
	}
	
	public void finalizeFlow() {
		System.out.println("Beende Flow");
	}
	
	public String finishRegistration() {
		return "finishRegistration";
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
}
