package de.christopherolbertz.flows.view;

import java.io.Serializable;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.flow.FlowScoped;
import javax.inject.Named;

@Named
@FlowScoped("confirmation")
public class ConfirmationView implements Serializable {
	private String key;
	private String secretKey;
	
	@PostConstruct
	public void createKey() {
		secretKey = UUID.randomUUID().toString();
		System.out.println("Der geheime Schlüssel zum Mitschreiben: " + secretKey);
	}

	public String checkKey() {
		if (key.equals(secretKey)) {
			return "confirmationSuccessful";
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Falsch!", "Der eingegebene Schlüssel ist nicht richtig!"));
			return null;
		}
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}