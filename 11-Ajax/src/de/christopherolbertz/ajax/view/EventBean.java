package de.christopherolbertz.ajax.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

@ManagedBean
@ViewScoped
public class EventBean {
	private int zahlQuersumme;
	private int quersumme;
	private String style;
	private String position; 
	
	public void berechneQuersumme(AjaxBehaviorEvent ajaxBehaviorEvent) {
		int zahl = zahlQuersumme;
		quersumme = 0;
		
		while (zahl > 0) {
			quersumme = quersumme + zahl % 10;
			zahl = zahl / 10;
		}
	}
	
	public void onMouseOver(AjaxBehaviorEvent ajaxBehaviorEvent) {
		style = "color:red;";
	}
	
	public void onMouseOut(AjaxBehaviorEvent ajaxBehaviorEvent) {
		style = "color:green;";
	}
	
	public void onDblClick(AjaxBehaviorEvent ajaxBehaviorEvent) {
		int positionZahl = getZufallszahl(1000);
		String farbe = farbenGenerator();
		position = "position:absolute;left:" + positionZahl + "px;"
				+ "background-color:" + farbe;
	}
	
	private int getZufallszahl(int grenze) {
		return (int)(Math.random() * grenze); 
	}
	
	private String farbenGenerator() {
		int zahl = getZufallszahl(5);
		String farbe = "";
		switch(zahl) {
			case 1: 	farbe = "red";
							break;
			case 2: 	farbe = "yellow";
			  				break;
			case 3: 	farbe = "green";
			  				break;
			case 4: 	farbe = "black";
			  				break;
			case 5: 	farbe = "blue";
			  				break;
		}
		return farbe;
	}
	
	public int getZahlQuersumme() {
		return zahlQuersumme;
	}

	public void setZahlQuersumme(int zahlQuersumme) {
		this.zahlQuersumme = zahlQuersumme;
	}

	public int getQuersumme() {
		return quersumme;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
}
