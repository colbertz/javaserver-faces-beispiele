package de.christopherolbertz.ajax.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;

@ManagedBean
@SessionScoped
public class CharCounterBean {
	private int counter;
	private String text1;
	private String text2;
	private String text3;
	
	public void countChars(ValueChangeEvent valueChangeEvent) {
		String newString = (String)valueChangeEvent.getNewValue();
		System.out.println();
		counter = newString.length(); 
	}
	
	public int getCounter() {
		return counter;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}

	public String getText3() {
		return text3;
	}

	public void setText3(String text3) {
		this.text3 = text3;
	}
}
