package de.christopherolbertz.ajax.view;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

/**
 * Diese Klasse erzeugt eine Zufallszahl.
 * @author Christopher Olbertz
 *
 */
@ManagedBean
public class NumberGeneratorBean {
	private double randomNumber = Math.random();
	private double range = 1.0;
	
	public void randomize(ActionEvent event) {
		randomNumber = range * Math.random();
	}

	public double getRandomNumber() {
		return randomNumber;
	}

	public void setRandomNumber(double randomNumber) {
		this.randomNumber = randomNumber;
	}

	public double getRange() {
		return range;
	}

	public void setRange(double range) {
		this.range = range;
	}
}
