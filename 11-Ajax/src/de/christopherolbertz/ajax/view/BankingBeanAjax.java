package de.christopherolbertz.ajax.view;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

/**
 * Die Bankklasse f�r die Ajax-Beispiele.
 * @author olbertz
 *
 */
@ManagedBean
public class BankingBeanAjax extends BankingBeanBase {
	/**
	 * Die Nachricht, die ausgegeben wird.
	 */
	  private String message = "";

	  public String getMessage() {
	    return(message);
	  }

	  public void setMessage(String message) {
	    this.message = message;
	  }

	  /**
	   * Pr�ft die Login-Daten und zeigt bei Eingabe richtiger Daten den Kontostand an.
	   */
	  public void showBalance(ActionEvent event) {
		  if (!getPassword().equals("geheim")) {
	    	message = "Falsches Passwort";
		  } else {
		      	CustomerSimpleMap customerMap = new CustomerSimpleMap();
		      	setCustomer(customerMap.findCustomer(getCustomerId()));
		      	if (getCustomer() == null) {
		      		message = "Unbekannter Kunde!";
		      	} else {
		      		message = String.format("Kontostand von %s %s ist $%,.2f",
		                        getCustomer().getFirstName(),
		                        getCustomer().getLastName(),
		                        getCustomer().getBalance());
		      	}
		  }
	  }
}

