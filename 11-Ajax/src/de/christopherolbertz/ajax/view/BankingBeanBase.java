package de.christopherolbertz.ajax.view;

/**
 * Diese Klasse simuliert eine Bank-Funktionialit�t
 * @author olbertz
 *
*/
public class BankingBeanBase {
	// ********************************************************* Attribute *************************************************
	/**
	 * Die Kundennummer.
	 */
	private String customerId;
	/**
	 * Das Passwort des Kunden.
	 */
	private String password;
	/**
	 * Der Kunde selbst.
	 */
	private Customer customer;

	// ********************************************************* Getter und Setter ******************************************
	public String getCustomerId(){
		return customerId;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}


	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
