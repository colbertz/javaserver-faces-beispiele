package de.christopherolbertz.spacegame.factories;

import de.christopherolbertz.spacegame.service.classes.PlanetServiceImpl;
import de.christopherolbertz.spacegame.service.classes.StarshipServiceImpl;
import de.christopherolbertz.spacegame.service.classes.UserServiceImpl;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;
import de.christopherolbertz.spacegame.service.interfaces.UserService;

public class ServiceFactory {
	public static UserService createUserService() {
		return new UserServiceImpl();
	}
	
	public static StarshipService createStarshipService() {
		return new StarshipServiceImpl();
	}
	
	public static PlanetService createPlanetService() {
		return new PlanetServiceImpl();
	}
}
