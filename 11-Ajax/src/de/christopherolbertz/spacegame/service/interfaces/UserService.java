package de.christopherolbertz.spacegame.service.interfaces;

import java.util.List;

import de.christopherolbertz.spacegame.model.interfaces.User;

public interface UserService {

	void delete(User user);

	User findUser(long userId);

	List<User> findAllUsers();

	void saveUser(User user);

	User findUserByName(String userName);

}
