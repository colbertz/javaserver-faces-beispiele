package de.christopherolbertz.spacegame.model.interfaces;

public interface Starship {
	public abstract long getStarshipId();
	public abstract void setStarshipId(long starshipId);
	public abstract String getStarshipName();
	public abstract void setStarshipName(String starshipName);
	public abstract double getDamage();
	public abstract void setDamage(double damage);
	public Planet getCurrentPlanet();
	public void setCurrentPlanet(Planet currentPlanet);
	//public boolean isFrigate();
	int getCrystalCost();
	int getMetalCost();
	void destroy();
	void damage(double attackStrength, double defendStrength);
	boolean isDestroyed();
	boolean isNotDestroyed();
	boolean isActive();
	void setActive(boolean active);
}