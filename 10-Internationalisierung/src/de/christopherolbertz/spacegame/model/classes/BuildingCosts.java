package de.christopherolbertz.spacegame.model.classes;

/**
 * Kaspelt die moeglichen Kosten in verschiedenen Rohstoffen. Dient sowohl der leichteren Rueckgabe von
 * mehreren Werten auf einmal als auch der leichterten Erweiterbarkeit um neue Rohstoffe.
 * @author christopher
 *
 */
public class BuildingCosts {
	private int metalCosts;
	private int crystalCosts;
	
	public BuildingCosts(final int metalCosts, final int crystalCosts) {
		super();
		this.metalCosts = metalCosts;
		this.crystalCosts = crystalCosts;
	}

	public int getMetalCosts() {
		return metalCosts;
	}
	
	public int getCrystalCosts() {
		return crystalCosts;
	}
}
