package de.christopherolbertz.dataevents.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;

@ManagedBean
@ViewScoped
public class RenameStarshipView {
	private String newStarshipName;
	private String oldStarshipName;
	private StarshipService starshipService;
	private Starship selectedStarship;
	
	@PostConstruct
	public void initialize() {
		starshipService = ServiceFactory.createStarshipService();
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		oldStarshipName = facesContext.getExternalContext().getRequestParameterMap().get("starshipName");
		final long starshipId = Long.parseLong(facesContext.getExternalContext().
				getRequestParameterMap().get("starshipId"));
		selectedStarship = starshipService.findStarship(starshipId);
	}
	
	public void renameStarship(ActionEvent actionEvent) {
		selectedStarship.setStarshipName(newStarshipName);
		starshipService.saveStarship(selectedStarship);
	}

	public String getNewStarshipName() {
		return newStarshipName;
	}

	public void setNewStarshipName(String newStarshipName) {
		this.newStarshipName = newStarshipName;
	}

	public String getOldStarshipName() {
		return oldStarshipName;
	}

	public void setOldStarshipName(String oldStarshipName) {
		this.oldStarshipName = oldStarshipName;
	}
}
