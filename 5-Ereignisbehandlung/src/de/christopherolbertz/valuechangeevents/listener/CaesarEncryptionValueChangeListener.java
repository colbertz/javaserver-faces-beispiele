package de.christopherolbertz.valuechangeevents.listener;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;

public class CaesarEncryptionValueChangeListener implements ValueChangeListener {
	private static final int ASCII_END_LOWERCASE_LETTERS = 122;
	private static final int ASCII_END_UPPERCASE_LETTERS = 90;
	private static final int SHIFT = 3;
	private static final int LETTERS_COUNT = 26;
	private static final String OUTPUT_TEXT_ID = "txtEncryped";
	
	@Override
	public void processValueChange(ValueChangeEvent valueChangeEvent) throws AbortProcessingException {
		final String newValue = (String)valueChangeEvent.getNewValue();
		final String encrypedText = encrypt(newValue);
		
		final UIComponent parentComponent = valueChangeEvent.getComponent().getParent();
		for (final UIComponent childComponent: parentComponent.getChildren()) {
			if (childComponent.getId().equals(OUTPUT_TEXT_ID)) {
				final HtmlOutputLabel outputLabel = (HtmlOutputLabel)childComponent;
				outputLabel.setValue(encrypedText);
			}
		}
	}

	private String encrypt(final String value) {
		String encrypedText = "";
		
		if (value != null) {
			final char[] text = value.toCharArray();
			for (int i = 0; i < text.length; i++) {
				char myChar = text[i];
				if (myChar >= ASCII_END_UPPERCASE_LETTERS - SHIFT  && myChar <= ASCII_END_UPPERCASE_LETTERS) {
					myChar = (char) (myChar + SHIFT - LETTERS_COUNT + 1);
				} else if (myChar >= ASCII_END_LOWERCASE_LETTERS - SHIFT && myChar <= ASCII_END_LOWERCASE_LETTERS) {
					myChar = (char) (myChar + SHIFT - LETTERS_COUNT + 1);
				} else {
					myChar = (char) (myChar + SHIFT);
				}
				text[i] = myChar;
			}
			encrypedText = String.valueOf(text);
		}
		
		return encrypedText;
	}
}
