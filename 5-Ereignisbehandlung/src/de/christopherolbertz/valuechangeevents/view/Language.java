package de.christopherolbertz.valuechangeevents.view;

public class Language {
	private String languageCode;
	private String languageName;
	
	public Language(String languageCode, String languageName) {
		super();
		this.languageCode = languageCode;
		this.languageName = languageName;
	}
	
	public String getLanguageCode() {
		return languageCode;
	}
	
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
	public String getLanguageName() {
		return languageName;
	}
	
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
}