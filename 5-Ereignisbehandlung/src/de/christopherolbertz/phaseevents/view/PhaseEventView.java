package de.christopherolbertz.phaseevents.view;

import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@ViewScoped
public class PhaseEventView {
	private int data;
	private static Logger logger = Logger.getLogger(
			"de.christopherolbertz.phaseevents.view.PhaseEventView");
	
	public void onSave(ActionEvent actionEvent) {
		logger.info("onSave()");
	}
	
	public void onCancel(ActionEvent actionEvent) {
		logger.info("onCancel");
	}
	
	public int getData() {
		return data;
	}
	
	public void setData(int data) {
		this.data = data;
	}
}
