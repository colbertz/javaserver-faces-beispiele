package de.christopherolbertz.spacegame.dao.interfaces;

import java.util.List;

import de.christopherolbertz.spacegame.model.interfaces.User;

public interface UserDao {
	public void delete(User user);
	public User findUser(long id);
	public List<User> getList();
	public void saveUser(User user);
}
