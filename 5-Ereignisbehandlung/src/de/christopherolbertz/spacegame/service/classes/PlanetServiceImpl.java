package de.christopherolbertz.spacegame.service.classes;

import java.util.ArrayList;
import java.util.List;

import de.christopherolbertz.spacegame.dao.classes.PlanetDaoImpl;
import de.christopherolbertz.spacegame.dao.classes.StarshipDaoImpl;
import de.christopherolbertz.spacegame.dao.interfaces.PlanetDao;
import de.christopherolbertz.spacegame.dao.interfaces.StarshipDao;
import de.christopherolbertz.spacegame.model.interfaces.Frigate;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;

public class PlanetServiceImpl implements PlanetService {
	private PlanetDao planetDao;
	private StarshipDao starshipDao;
	
	public PlanetServiceImpl() {
		planetDao = PlanetDaoImpl.getInstance();
		starshipDao = StarshipDaoImpl.getInstance();
	}
	
	public void delete(final Planet planet){
		planetDao.delete(planet);
	}
	
	@Override
	public void delete(final int planetId){
		final Planet planet = findPlanet(planetId);
		planetDao.delete(planet);
	}
	
	@Override
	public Planet findPlanet(final long id) {
		return planetDao.findPlanet(id);
	}
	
	@Override
	public List<Planet> getAllPlanets() {
		return planetDao.findAllPlanets();
	}
	
	@Override
	public void savePlanet(final Planet planet) {
		planetDao.savePlanet(planet);
	}
	
	@Override
	public void reduceMetalStorage(final int amount, final Planet planet) {
		planet.setMetalStorage(planet.getMetalStorage() - amount);
	}
	
	@Override
	public void reduceCrystalStorage(final int amount, final Planet planet) {
		planet.setCrystalStorage(planet.getCrystalStorage() - amount);
	}

	@Override
	public List<Planet> findPlanetsByUser(User user) {
		return planetDao.findPlanetsByUser(user);
	}
	
	private List<Frigate> filterFrigates(final List<Starship> starships) {
		final List<Frigate> frigates = new ArrayList<>();
		
		for (final Starship starship: starships) {
			if (starship instanceof Frigate) {
				final Frigate frigate = (Frigate)starship;
				frigates.add(frigate);
			}
		}
		
		return frigates;
	}
	
	@Override
	public boolean attackPlanet(final Planet targetPlanet, final List<Starship> attackingStarships) {
		final List<Starship> starshipsOfEnemy = targetPlanet.getStarshipsOnPlanet();
		final List<Frigate> defendingFrigates = filterFrigates(starshipsOfEnemy);
		final List<Frigate> attackingFrigates = filterFrigates(attackingStarships);
		
		int defendingStarshipCounter = 0;
		int attackingStarshipCounter = 0;
		Frigate attackingFrigate = attackingFrigates.get(attackingStarshipCounter);
		Frigate defendingFrigate = defendingFrigates.get(defendingStarshipCounter);
		
		while (attackingFrigate != null && defendingFrigate != null) {
			final double attackStrength = attackingFrigate.getAttackStrength();
			final double defendStrength = defendingFrigate.getDefendStrength();
			
			while (attackingFrigate.isNotDestroyed() && defendingFrigate.isNotDestroyed()) {
				attackingFrigate.damage(defendStrength, attackStrength);
				defendingFrigate.damage(attackStrength, defendStrength);
			}
			
			if (defendingFrigate.isDestroyed())  {
				defendingStarshipCounter++;
				if (defendingStarshipCounter < starshipsOfEnemy.size()) {
					defendingFrigate = (Frigate)starshipsOfEnemy.get(defendingStarshipCounter);
				} else {
					defendingFrigate = null;
				}
			} else if (attackingFrigate.isDestroyed()) {
				attackingStarshipCounter++;
				if (attackingStarshipCounter < attackingStarships.size()) {
					attackingFrigate = (Frigate)attackingStarships.get(attackingStarshipCounter);
				} else {
					attackingFrigate = null;
				}
			}
		}
		
		updateStarshipAfterBattle(attackingFrigates);
		updateStarshipAfterBattle(defendingFrigates);
		
		if (attackingFrigate == null) {
			return false;
		} else {
			return true;
		}
	}
	
	private void updateStarshipAfterBattle(List<Frigate> frigates) {
		for (final Frigate frigate: frigates) {
			starshipDao.saveStarship(frigate);
		}
	}
}
