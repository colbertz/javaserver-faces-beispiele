package de.christopherolbertz.spacegame.model.interfaces;

public interface TransportShip {
	public int getUsedSpace();
	public void setUsedSpace(int usedSpace);
	public int getMaxStockroom();
	public void setMaxStockroom(int maxStockroom);
}
