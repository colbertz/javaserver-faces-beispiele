package de.christopherolbertz.spacegame.model.classes;

import java.util.ArrayList;
import java.util.List;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.User;

/**
 * Eine einfache Klasse fuer die Benutzer der Webseite.
 * @author Christopher
 *
 */
public class UserImpl implements User {
	private long userId;
	private String username;
	private String emailaddress;
	private String password;
	private boolean holidayModus;
	private List<Planet> myPlanets = new ArrayList<Planet>();
	
	public UserImpl() {
		super();
	}
	
	public UserImpl(String username, String emailaddress,
			String password) {
		super();
		this.username = username;
		this.emailaddress = emailaddress;
		this.password = password;
	}

	public UserImpl(int userId, String username, String emailaddress, String password) {
		this(username, emailaddress, password);
		this.userId = userId;
	}

	@Override
	public long getUserId() {
		return userId;
	}
	
	@Override
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Override
	public String getUsername() {
		return username;
	}
	
	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getEmailaddress() {
		return emailaddress;
	}
	
	@Override
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean isHolidayModus() {
		return holidayModus;
	}

	@Override
	public void setHolidayModus(boolean holidayModus) {
		this.holidayModus = holidayModus;
	}

	@Override
	public List<Planet> getMyPlanets() {
		return myPlanets;
	}

	@Override
	public void setMyPlanets(List<Planet> myPlanets) {
		this.myPlanets = myPlanets;
	}
}
