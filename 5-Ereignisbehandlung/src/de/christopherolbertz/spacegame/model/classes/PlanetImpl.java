package de.christopherolbertz.spacegame.model.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.User;

public class PlanetImpl implements Planet, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3946548896828194347L;
	private long planetId;
	private String planetName;
	private int metalStorage;
	private int crystalStorage;
	private List<Starship> starshipsOnPlanet = new ArrayList<Starship>();
	private int maxStarshipCapacity = 10;
	private User owner;
	//private List<Building> buildingsOnPlanet;
	
	@Override
	public void landStarship(final Starship starship) {
		try {
			starshipsOnPlanet.add(starship);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void landStarshipFleet(final List<Starship> starships) {
		starshipsOnPlanet.addAll(starships);
	}
	
	@Override
	public void reduceMetalStorage(final int metalAmount) {
		metalStorage = metalStorage - metalAmount;
	}
	
	public PlanetImpl(final String planetName, final int metalStorage, final int crystalStorage) {
		super();
		this.planetName = planetName;
		this.metalStorage = metalStorage;
		this.crystalStorage = crystalStorage;
	}
	
	public PlanetImpl(final int planetId, final String planetName, final int metalStorage, final int crystalStorage) {
		this(planetName, metalStorage, crystalStorage);
		this.planetId = planetId;
	}

	public PlanetImpl() {
		super();
	}

	@Override
	public void reduceCrystalStorage(final int crystalAmount) {
		crystalStorage = crystalStorage - crystalAmount;
	}

	@Override
	public long getPlanetId() {
		return planetId;
	}
	
	@Override
	public void setPlanetId(long planetId) {
		this.planetId = planetId;
	}
	
	@Override
	public String getPlanetName() {
		return planetName;
	}
	
	@Override
	public void setPlanetName(String planetName) {
		this.planetName = planetName;
	}

	@Override
	public int getMetalStorage() {
		return metalStorage;
	}

	@Override
	public void setMetalStorage(int metalStorage) {
		this.metalStorage = metalStorage;
	}

	@Override
	public int getCrystalStorage() {
		return crystalStorage;
	}

	@Override
	public void setCrystalStorage(int crystalStorage) {
		this.crystalStorage = crystalStorage;
	}
	
	@Override
	public int getMaxStarshipCapacity() {
		return maxStarshipCapacity;
	}

	@Override
	public void setMaxStarshipCapacity(int maxStarshipCapacity) {
		this.maxStarshipCapacity = maxStarshipCapacity;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (planetId ^ (planetId >>> 32));
		result = prime * result + ((planetName == null) ? 0 : planetName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanetImpl other = (PlanetImpl) obj;
		if (planetId != other.planetId)
			return false;
		if (planetName == null) {
			if (other.planetName != null)
				return false;
		} else if (!planetName.equals(other.planetName))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return planetName;
	}

	@Override
	public List<Starship> getStarshipsOnPlanet() {
		return starshipsOnPlanet;
	}

	@Override
	public void setStarshipsOnPlanet(List<Starship> starshipsOnPlanet) {
		this.starshipsOnPlanet = starshipsOnPlanet;
	}
	
	@Override
	public User getOwner() {
		return owner;
	}
	
	@Override
	public void setOwner(User owner) {
		this.owner = owner;
	}
}
