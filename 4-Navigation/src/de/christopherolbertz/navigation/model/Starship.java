package de.christopherolbertz.navigation.model;

public class Starship {
	private int starshipId;
	private String name;
	private double damage;
	
	public Starship(int starshipId, String name, double damage) {
		super();
		this.starshipId = starshipId;
		this.name = name;
		this.damage = damage;
	}

	public int getStarshipId() {
		return starshipId;
	}

	public void setStarshipId(int starshipId) {
		this.starshipId = starshipId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
	}
}
