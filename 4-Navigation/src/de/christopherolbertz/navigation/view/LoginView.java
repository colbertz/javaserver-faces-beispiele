package de.christopherolbertz.navigation.view;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Diese Klasse repraesentiert das Fenster, in welchem der Benutzer sich ins Spiel einloggen kann.
 * @author Christopher
 *
 */

@ManagedBean
@SessionScoped
public class LoginView {
	private String username;
	private String password;
	private int wrongPasswordCounter;
	
	public String toRegistration() {
		return "registration";
	}
	
	public String login() {
		if (!username.equals("stl")) {
			return "wrongUsername";
		} else {
			if (password.equals("geheim")) {
				return "loginSuccessful";
			} else {
				wrongPasswordCounter++;
				if (wrongPasswordCounter == 3) {
					return "accountLocked";
				} else {
					return "wrongPassword";
				}
			}
		}
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
