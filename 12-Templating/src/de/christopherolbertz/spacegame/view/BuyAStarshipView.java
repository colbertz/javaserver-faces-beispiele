package de.christopherolbertz.spacegame.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;

import de.christopherolbertz.spacegame.constants.SpaceGameConstants;
import de.christopherolbertz.spacegame.model.classes.BuildingCosts;
import de.christopherolbertz.spacegame.model.classes.FrigateImpl;
import de.christopherolbertz.spacegame.model.classes.SpyShipImpl;
import de.christopherolbertz.spacegame.model.classes.TransportShipImpl;
import de.christopherolbertz.spacegame.model.interfaces.Frigate;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.SpyShip;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.TransportShip;
import de.christopherolbertz.spacegame.service.classes.PlanetServiceImpl;
import de.christopherolbertz.spacegame.service.classes.StarshipServiceImpl;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;
import de.christopherolbertz.spacegame.utils.JsfUtils;
import de.christopherolbertz.spacegame.utils.RandomUtils;
import de.christopherolbertz.spacegame.utils.SpaceGameUtils;

@ManagedBean
@ViewScoped
public class BuyAStarshipView implements Serializable {
	private static final long serialVersionUID = -4309349799338463368L;
	private int shipCount;
	private String starshipType;
	private int metalCosts;
	private int crystalCosts;
	private PlanetService planetService;
	private StarshipService starshipService;
	private Planet planet;
	
	@PostConstruct
	public void initialize() {
		starshipService = new StarshipServiceImpl();
		planetService = new PlanetServiceImpl();
		planet = (Planet)JsfUtils.getBeanAttribute("selectedPlanet", "tactical", Planet.class);
		starshipType = "TransportShip";
	}
	
	public void calculateCosts(AjaxBehaviorEvent ajaxBehaviorEvent) {
		BuildingCosts buildingCosts = null;
		
		if (TransportShip.class.toString().endsWith(starshipType)) {
			buildingCosts = SpaceGameUtils.determineTransportShipBuildingCosts(shipCount);
		} else if (SpyShip.class.toString().endsWith(starshipType)) {
			buildingCosts = SpaceGameUtils.determineSpyShipBuildingCosts(shipCount);
		} else if (Frigate.class.toString().endsWith(starshipType)) {
			buildingCosts = SpaceGameUtils.determineFrigateBuildingCosts(shipCount);
		}
		
		metalCosts = buildingCosts.getMetalCosts();
		crystalCosts = buildingCosts.getCrystalCosts();
	}
	
	public void buyShips(ActionEvent event) {		
		planetService.reduceCrystalStorage(crystalCosts, planet);
		planetService.reduceMetalStorage(metalCosts, planet);
		List<Starship> newStarships = new ArrayList<Starship>();
		
		for (int i = 0; i < shipCount; i++) {
			long starshipId = RandomUtils.createRandomStarshipId();
			if (TransportShip.class.toString().endsWith(starshipType)) {
				newStarships.add(new TransportShipImpl("Transportship " + starshipId, planet, 0, SpaceGameConstants.STANDARD_STOCKROOM));
			} else if (SpyShip.class.toString().endsWith(starshipType)) {
				newStarships.add(new SpyShipImpl("Spyship " + starshipId, planet, SpaceGameConstants.STANDARD_SENSOR_COUNT, SpaceGameConstants.STANDARD_CAMOUFLAGE));
			} else if (Frigate.class.toString().endsWith(starshipType)) {
				newStarships.add(new FrigateImpl("Frigate " + starshipId, planet, SpaceGameConstants.STANDARD_CANON_COUNT, SpaceGameConstants.STANDARD_SHIELDS));
			}
			starshipService.stationStarshipOnPlanet(newStarships.get(i), planet);			
		}
		
		starshipService.saveStarships(newStarships);
	}
	
	public int getShipCount() {
		return shipCount;
	}

	public void setShipCount(int shipCount) {
		this.shipCount = shipCount;
	}

	public String getStarshipType() {
		return starshipType;
	}

	public void setStarshipType(String starshipType) {
		this.starshipType = starshipType;
	}

	public int getMetalCosts() {
		return metalCosts;
	}

	public void setMetalCosts(int metalCosts) {
		this.metalCosts = metalCosts;
	}

	public int getCrystalCosts() {
		return crystalCosts;
	}

	public void setCrystalCosts(int crystalCosts) {
		this.crystalCosts = crystalCosts;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}
}
