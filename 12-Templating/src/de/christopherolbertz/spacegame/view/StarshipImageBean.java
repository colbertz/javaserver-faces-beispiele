package de.christopherolbertz.spacegame.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;

@ManagedBean
@NoneScoped
public class StarshipImageBean {
	@PostConstruct
	public void initialize() {
		
	}
	
	public String determineBla(String blub) {
		return blub + "bla";
	}
	
	public String determineStarshipImage(final String starshipName) {
		return "bla";
	}
}
