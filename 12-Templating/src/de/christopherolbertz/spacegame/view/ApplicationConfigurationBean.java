package de.christopherolbertz.spacegame.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class ApplicationConfigurationBean {
	private boolean inDevelopment;
	
	@PostConstruct
	public void initialize() {
		inDevelopment = true;
	}
	
	public boolean isInDevelopment() {
		return inDevelopment;
	}
}
