package de.christopherolbertz.spacegame.model.interfaces;

public interface SpyShip {
	public int getSensorCount();
	public void setSensorCount(int sensorCount);
	public int getCamouflage();
	public void setCamouflage(int camouflage);
}
