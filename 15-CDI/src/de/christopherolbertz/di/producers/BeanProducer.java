package de.christopherolbertz.di.producers;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import javax.enterprise.inject.Produces;

import de.christopherolbertz.di.qualifier.MyDate;
import de.christopherolbertz.di.qualifier.MyQualifier;
import de.christopherolbertz.di.qualifier.RandomWithService;
import de.christopherolbertz.di.view.RandomService;
import de.christopherolbertz.di.view.Service;
import de.christopherolbertz.di.view.ServiceImpl;

public class BeanProducer {
	@Produces
	@de.christopherolbertz.di.qualifier.Random
	public int produceRandom() {
		return new Random().nextInt(1000);
	}
	
	@Produces
	@MyDate
	public Date produceCurrentDate() {
		return new Date();
	}
	
	@Produces
	@RandomWithService
	public int produceRandomWithService(@MyQualifier Service service) {
		if (service instanceof RandomService) {
			return new Random().nextInt(100) + service.getInteger();
		} else {
			return service.getInteger() * (-1);
		}
	}
}
