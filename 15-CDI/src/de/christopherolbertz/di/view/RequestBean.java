package de.christopherolbertz.di.view;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class RequestBean {
	public String getText() {
		return "Ich bin die Request-Bean";
	}
}
