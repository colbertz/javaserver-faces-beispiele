package de.christopherolbertz.komponenten.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

@ManagedBean
public class DemoSelectMany {
	private List<SelectItem> foodList;
	private List<SelectItem> movieList;
	private List<String> selectedFood;
	private List<String> selectedMovies;
	private List<String> selectedSports;
	
	@PostConstruct
	public void initialize() {
		foodList = new ArrayList<SelectItem>();
		foodList.add(new SelectItem("Pizza"));
		foodList.add(new SelectItem("Bohnensuppe"));
		foodList.add(new SelectItem("Hamburger"));
		foodList.add(new SelectItem("Griechischer Salat"));
		
		movieList = new ArrayList<SelectItem>();
		movieList.add(new SelectItem("Indiana Jones"));
		movieList.add(new SelectItem("Star Trek"));
		movieList.add(new SelectItem("Star Wars"));
	}

	private String convertToString(List<String> myList) {
		String result = "";
		for (String myString: myList) {
			result = result + " " + myString;
		}
		
		return result;
	}
	
	public String getFoodStrings() {
		if (selectedFood != null) {
			return convertToString(selectedFood);
		} else {
			return "";
		}
	}
	
	public String getSportStrings() {
		if (selectedSports != null) {
			return convertToString(selectedSports);
		} else {
			return "";
		}
	}
	
	public String getMovieStrings() {
		if (selectedMovies != null) {
			return convertToString(selectedMovies);
		} else {
			return "";
		}
	}
	
	public List<SelectItem> getFoodList() {
		return foodList;
	}

	public void setFoodList(List<SelectItem> foodList) {
		this.foodList = foodList;
	}

	public List<SelectItem> getMovieList() {
		return movieList;
	}

	public void setMovieList(List<SelectItem> movieList) {
		this.movieList = movieList;
	}


	public List<String> getSelectedFood() {
		return selectedFood;
	}


	public void setSelectedFood(List<String> selectedFood) {
		this.selectedFood = selectedFood;
	}


	public List<String> getSelectedMovies() {
		return selectedMovies;
	}


	public void setSelectedMovies(List<String> selectedMovies) {
		this.selectedMovies = selectedMovies;
	}


	public List<String> getSelectedSports() {
		return selectedSports;
	}


	public void setSelectedSports(List<String> selectedSports) {
		this.selectedSports = selectedSports;
	}
}