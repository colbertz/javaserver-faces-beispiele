package de.christopherolbertz.komponenten.view;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class DemoInputSecret {
	/**
	 * Das Passwort zum Einloggen.
	 */
	private String password;
	/**
	 * Der Name zum Einloggen.
	 */
	private String nickname;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
}