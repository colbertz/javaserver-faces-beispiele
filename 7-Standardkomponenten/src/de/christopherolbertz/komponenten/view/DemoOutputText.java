package de.christopherolbertz.komponenten.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class DemoOutputText {
	private String firstName;
	private String lastName;
	private String htmlText;
	
	@PostConstruct
	public void initialize() {
		firstName = "Phil";
		lastName = "Harmonie";
		htmlText = "Hallo, liebe <b>Kursteilnehmer</b>";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getHtmlText() {
		return htmlText;
	}

	public void setHtmlText(String htmlText) {
		this.htmlText = htmlText;
	}
}
