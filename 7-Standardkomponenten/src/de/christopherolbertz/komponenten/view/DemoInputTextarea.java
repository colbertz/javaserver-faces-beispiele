package de.christopherolbertz.komponenten.view;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class DemoInputTextarea {
	private String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}