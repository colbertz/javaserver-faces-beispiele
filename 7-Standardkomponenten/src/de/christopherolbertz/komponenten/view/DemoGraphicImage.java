package de.christopherolbertz.komponenten.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@ViewScoped
public class DemoGraphicImage {   
	private List<String> imagePaths;
	private List<String> countryNames;
	private int flagCounter;
	
	@PostConstruct
	public void initialize() {
		imagePaths = new ArrayList<>();
		imagePaths.add("flags/ireland.png");
		imagePaths.add("flags/france.png");
		imagePaths.add("flags/germany.png");
		imagePaths.add("flags/spain.png");
		imagePaths.add("flags/sweden.png");
		imagePaths.add("flags/norway.png");
		imagePaths.add("flags/unitedkingdom.png");
		imagePaths.add("flags/malta.png");
		
		countryNames = new ArrayList<>();
		countryNames.add("Irland");
		countryNames.add("Frankreich");
		countryNames.add("Deutschland");
		countryNames.add("Spanien");
		countryNames.add("Schweden");
		countryNames.add("Norwegen");
		countryNames.add("Vereinigtes Königreich");
		countryNames.add("Malta");
	}
	
	public void onChangeFlag(ActionEvent actionEvent) {
		if (flagCounter < imagePaths.size() - 1) {
			flagCounter++;
		} else {
			flagCounter = 0;
		}
	}
	
	public String getImagePath() {
		return imagePaths.get(flagCounter);
	}
	
	public String getCountryName() {
		return countryNames.get(flagCounter);
	}
}