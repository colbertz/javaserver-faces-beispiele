package de.christopherolbertz.komponenten.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class DemoOutputLink {
	private String name;
	private String company;
	private String website;
	
	@PostConstruct
	public void initialize() {
		name = "Claire Grube";
		company = "Ecosia";
		website = "http://www.ecosia.de";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
}