package de.christopherolbertz.komponenten.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.christopherolbertz.komponenten.model.Country;

public class CountryService {
	private List<Country> countryList;
	private static CountryService countryService;
	
	private CountryService() {
		countryList = new ArrayList<Country>();      
		countryList.add(new Country("DE", "Deutschland", 82792351, "germany"));
		countryList.add(new Country("FR", "Frankreich", 66991000, "france"));
		countryList.add(new Country("ES", "Spanien",  46549045, "spain"));
		countryList.add(new Country("ML", "Malta",  433300, "malta"));
		countryList.add(new Country("GR", "Griechenland",  131957, "greece"));
		countryList.add(new Country("JP", "Japan",  377835, "japan", false));
	}
	
	public static CountryService getInstance() {
		if (countryService == null) {
			countryService = new CountryService();
		}
		
		return countryService;
	}
	
	public List<Country> findAllCountries() {
		return Collections.unmodifiableList(countryList);
	}

	public void removeCountry(int selectedIndex) {
		countryList.remove(selectedIndex);
	}

	public int findPopulation(String countryCode) {
		Country country = findCountryByCode(countryCode);
		if (country != null) {
			return country.getPopulation();
		}
		return 0;
	}
	
	private Country findCountryByCode(final String countryCode) {
		for (final Country country: countryList) {
			if (country.getIsoCode().equals(countryCode)) {
				return country;
			}
		}
		
		return null;
	}

	public void updatePopulation(String countryCode, int newPopulation) {
		Country country = findCountryByCode(countryCode);
		if (country != null) {
			country.setPopulation(newPopulation);
		}
	}
}
