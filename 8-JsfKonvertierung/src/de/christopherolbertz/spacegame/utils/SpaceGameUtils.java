package de.christopherolbertz.spacegame.utils;

import de.christopherolbertz.spacegame.factories.ModelFactory;
import de.christopherolbertz.spacegame.model.classes.BuildingCosts;
import de.christopherolbertz.spacegame.model.interfaces.Starship;

public class SpaceGameUtils {
	public static final int getFrigateCrystalCosts(int frigateCount) {
		final Starship frigate = ModelFactory.createFrigate(1, "", null, 0, 0);
		return frigate.getCrystalCost() * frigateCount;
	}
	
	public static BuildingCosts determineSpyShipBuildingCosts(final int spyshipCount) {
		final Starship spyShip = ModelFactory.createSpyShip(1, "", null, 0, 0);
		final int metalCosts = spyShip.getMetalCost() * spyshipCount;
		final int crystalCosts = spyShip.getCrystalCost() * spyshipCount;
		final BuildingCosts buildingCosts = new BuildingCosts(metalCosts, crystalCosts);
		return buildingCosts;
	}
	
	public static BuildingCosts determineTransportShipBuildingCosts(final int transportShipCount) {
		final Starship transportShip = ModelFactory.createTransportShip("", null, 0, 0);
		final int metalCosts = transportShip.getMetalCost() * transportShipCount;
		final int crystalCosts = transportShip.getCrystalCost() * transportShipCount;
		final BuildingCosts buildingCosts = new BuildingCosts(metalCosts, crystalCosts);
		return buildingCosts;
	}
	
	public static BuildingCosts determineFrigateBuildingCosts(final int frigateCount) {
		final Starship frigate = ModelFactory.createFrigate(1, "", null, 0, 0);
		final int metalCosts = frigate.getMetalCost() * frigateCount;
		final int crystalCosts = frigate.getCrystalCost() * frigateCount;
		final BuildingCosts buildingCosts = new BuildingCosts(metalCosts, crystalCosts);
		return buildingCosts;
	}
}
