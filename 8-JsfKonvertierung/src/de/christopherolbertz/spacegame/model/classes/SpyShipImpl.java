package de.christopherolbertz.spacegame.model.classes;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.SpyShip;

/**
 * Ein Spionageschiff. Die Anzahl der Sensoren gibt an, wie nah das Schiff an einem Planeten sein muss, damit es 
 * Informationen ueber diesen sammeln kann. Die Tarnung gibt an, bis zur welcher Staerke gegnerischer Sensoren das
 * Schiff unentdeckt bleibt.
 * @author Christopher
 *
 */
public class SpyShipImpl extends StarshipImpl implements SpyShip {
	private int sensorCount;
	private int camouflage;
	
	public SpyShipImpl(final String name, final Planet currentPlanet, 
			final int sensorCount, final int camouflage) {
		super(name, currentPlanet);
		this.sensorCount = sensorCount;
		this.camouflage = camouflage;
	}
	
	public SpyShipImpl(final long starshipId, final String name, final Planet currentPlanet, 
			final int sensorCount, final int camouflage) {
		super(starshipId, name, currentPlanet);
		this.sensorCount = sensorCount;
		this.camouflage = camouflage;
	}

	public int getSensorCount() {
		return sensorCount;
	}

	public void setSensorCount(int sensorCount) {
		this.sensorCount = sensorCount;
	}

	public int getCamouflage() {
		return camouflage;
	}

	public void setCamouflage(int camouflage) {
		this.camouflage = camouflage;
	}
	
	@Override
	public int getCrystalCost() {
		return 40;
	}

	@Override
	public int getMetalCost() {
		return 20;
	}
}
