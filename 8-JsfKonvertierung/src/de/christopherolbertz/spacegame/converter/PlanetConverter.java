package de.christopherolbertz.spacegame.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;

@FacesConverter("planetConverter")
public class PlanetConverter implements Converter<Planet> {

	@Override
	public Planet getAsObject(FacesContext facesContext, 
			UIComponent source, String value) {
		final PlanetService planetService = ServiceFactory.createPlanetService();
		//final long planetId = Long.parseLong(value);
		final Planet planet = planetService.findPlanetByName(value);
		return planet;
	}

	@Override
	public String getAsString(FacesContext facesContext, 
			UIComponent source, Planet value) {
		return value.toString();
	}

}
