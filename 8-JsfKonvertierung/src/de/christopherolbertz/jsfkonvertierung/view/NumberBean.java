package de.christopherolbertz.jsfkonvertierung.view;

import javax.faces.bean.ManagedBean;

import de.christopherolbertz.jsfkonvertierung.spacegame.utils.RandomUtils;

@ManagedBean
public class NumberBean {
	private int inputValue;
	
	public double getDoubleValue() {
		return 17.12345678;
	}
	
	public double getValue() {
		return RandomUtils.randomDouble();
	}
	
	public int getInputValue() {
		return inputValue;
	}
	
	public void setInputValue(int inputValue) {
		this.inputValue = inputValue;
	}
}
