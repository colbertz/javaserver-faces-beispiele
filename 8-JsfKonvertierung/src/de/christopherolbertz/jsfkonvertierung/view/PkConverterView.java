package de.christopherolbertz.jsfkonvertierung.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import de.christopherolbertz.jsfkonvertierung.converter.PkConverter;
import de.christopherolbertz.jsfkonvertierung.model.Pk;

@ManagedBean
public class PkConverterView {
	private Pk pk;
	private Pk pk2;
	
	@PostConstruct
	public void bla() {
		System.out.println("*********");
	}

	public Pk getPk() {
		return pk;
	}

	public void setPk(Pk pk) {
		this.pk = pk;
	}
	
	public PkConverter getPkConverter() {
		return new PkConverter();
	}

	public Pk getPk2() {
		return pk2;
	}

	public void setPk2(Pk pk2) {
		this.pk2 = pk2;
	}
}   
