package de.christopherolbertz.jsfkonvertierung.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 * Eine kleine Taschenrechner-Bean, die die Konvertierung von einfachen Datentypen 
 * demonstriert.
 * @author Christopher
 *
 */
@ManagedBean
@ViewScoped
public class CalculatorBean {
	private int number2;
	private int number1;
	private double result;
	private String operator;
	
	public void calculate(ActionEvent actionEvent) {
		if (operator.equals("+")) {
			result = number1 + number2;
		} else if (operator.equals("-")) {
			result = number1 - number2;
		} else if (operator.equals("*")) {
			result = number1 * number2;
		} else {
			result = number1 / (double)number2;
		}
	}
	
	public int getNumber2() {
		return number2;
	}
	
	public void setNumber2(int number2) {
		this.number2 = number2;
	}
	
	public int getNumber1() {
		return number1;
	}
	
	public void setNumber1(int number1) {
		this.number1 = number1;
	}
	
	public double getResult() {
		return result;
	}
	
	public void setResult(double result) {
		this.result = result;
	}
	
	public String getOperator() {
		return operator;
	}
	
	public void setOperator(String operator) {
		this.operator = operator;
	}
}
