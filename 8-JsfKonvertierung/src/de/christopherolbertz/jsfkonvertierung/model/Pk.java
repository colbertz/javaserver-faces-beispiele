package de.christopherolbertz.jsfkonvertierung.model;

import java.util.Calendar;

/**
 * Repr�sentiert eine Personenkennzifer bei der Bundeswehr.
 * @author Christopher
 *
 */
public class Pk {
	/**
	 * Das Geburtsdatum des Soldaten.
	 */
	private Calendar geburtsdatum;
	/**
	 * Der erste Buchstabe des Nachnamens.
	 */
	private char ersterBuchstabe;
	/**
	 * Die Id des Kreiswehrersatzamtes.
	 */
	private int kreiswehrersatzamt;
	/**
	 * Die laufende Nummer der Pk.
	 */
	private int laufendeNummer;
	
	public Pk(Calendar geburtsdatum, char ersterBuchstabe,
			int kreiswehrersatzamt, int laufendeNummer) {
		super();
		this.geburtsdatum = geburtsdatum;
		this.ersterBuchstabe = ersterBuchstabe;
		this.kreiswehrersatzamt = kreiswehrersatzamt;
		this.laufendeNummer = laufendeNummer;
	}

	public Calendar getGeburtsdatum() {
		return geburtsdatum;
	}
	
	public void setGeburtsdatum(Calendar geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}
	
	public char getErsterBuchstabe() {
		return ersterBuchstabe;
	}
	
	public void setErsterBuchstabe(char ersterBuchstabe) {
		this.ersterBuchstabe = ersterBuchstabe;
	}
	
	public int getKreiswehrersatzamt() {
		return kreiswehrersatzamt;
	}
	
	public void setKreiswehrersatzamt(int kreiswehrersatzamt) {
		this.kreiswehrersatzamt = kreiswehrersatzamt;
	}
	
	public int getLaufendeNummer() {
		return laufendeNummer;
	}
	
	public void setLaufendeNummer(int laufendeNummer) {
		this.laufendeNummer = laufendeNummer;
	}
	
	@Override
	public String toString() {
		String day = String.valueOf(geburtsdatum.get(Calendar.DAY_OF_MONTH));
		if (Integer.parseInt(day) < 10) {
			day = "0" + day;
		}
		String month = String.valueOf(geburtsdatum.get(Calendar.MONTH));
		if (Integer.parseInt(month) < 10) {
			month = "0" + month;
		}
		
		final String year = String.valueOf(geburtsdatum.get(Calendar.YEAR));
		return day + month + year + "-" + ersterBuchstabe + "-" + 
			   kreiswehrersatzamt + laufendeNummer;
	}
}
