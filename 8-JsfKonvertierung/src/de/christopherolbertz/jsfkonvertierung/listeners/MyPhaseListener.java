package de.christopherolbertz.jsfkonvertierung.listeners;

import java.util.logging.Logger;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class MyPhaseListener implements PhaseListener {
	private static Logger logger = Logger.getLogger(
			"de.christopherolbertz.actionevents.listeners.MyPhaseListener");

	@Override
	public void afterPhase(PhaseEvent phaseEvent) {
		logger.info("Nach der Phase: " + phaseEvent.getPhaseId());
	}

	@Override
	public void beforePhase(PhaseEvent phaseEvent) {
		logger.info("Vor der Phase: " + phaseEvent.getPhaseId());
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

}
