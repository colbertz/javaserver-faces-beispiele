package de.stl.saar.internetentw1.di.view;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value ="test")
@RequestScoped
//@ManagedBean
//@SessionScoped
public class Test implements Serializable{

    public Test() {
    }

    public Test(String output) {
        this.output = output;
    }

    private String output;

    public String getOutput() {
        output = "xxxxx";
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}