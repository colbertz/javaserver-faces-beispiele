package de.stl.saar.internetentw1.di.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ManagedPropertiesView implements Serializable {
	@ManagedProperty(value = "66")
	private int anyNumber;
	@ManagedProperty(value = "Hallo")
	private String anyString;
	@ManagedProperty(value = "#{injectedBean}")
	private InjectedBean injectedBean;
	
	public int getAnyNumber() {
		return anyNumber;
	}
	public void setAnyNumber(int anyNumber) {
		this.anyNumber = anyNumber;
	}
	public String getAnyString() {
		return anyString;
	}
	public void setAnyString(String anyString) {
		this.anyString = anyString;
	}
	public InjectedBean getInjectedBean() {
		return injectedBean;
	}
	public void setInjectedBean(InjectedBean injectedBean) {
		this.injectedBean = injectedBean;
	}
	
}
