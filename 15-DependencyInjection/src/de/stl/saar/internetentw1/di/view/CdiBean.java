package de.stl.saar.internetentw1.di.view;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class CdiBean implements Serializable {
	private String text;
	
	public CdiBean() {
		text = "Hallo CDI!";
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}
