package de.stl.saar.internetentw1.di.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class InjectedBean {
	@ManagedProperty(value = "1234567")
	private long anyLong;
	
	public long getAnyLong() {
		return anyLong;
	}
	
	public void setAnyLong(long anyLong) {
		this.anyLong = anyLong;
	}
}
