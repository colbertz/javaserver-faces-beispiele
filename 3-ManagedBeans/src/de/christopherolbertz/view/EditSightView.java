package de.christopherolbertz.view;

import java.util.HashMap;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.Application;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import de.christopherolbertz.model.classes.CityImpl;
import de.christopherolbertz.model.interfaces.City;

@ManagedBean(name = "editSightView")
public class EditSightView {
	private long sightId;
	private String sightName;
	private double entranceFee;
	private City city;
	/**
	 * Zugriffe auf Arrays sind moeglich wie aus Java gewohnt: #{array[0]}.
	 */
	private String[] array;
	/**
	 * Eine Hashmap mit einfachen Daten zur Darstellung auf der xhtml-Seite.
	 */
	private Map<String, String> map;
	
	public EditSightView() {
		sightId = 1;
		sightName = "Akropolis";
		entranceFee = 12.5;
		city = new CityImpl(5, "Athen");
		
		map = new HashMap<String, String>();
		map.put("eins", "Erster Map-Eintrag");
		map.put("zwei", "Zweiter Map-Eintrag");
		map.put("drei", "Dritter Map-Eintrag");
		array = new String[]{"eins", "zwei", "drei"};
	}
	
	public double incrementEntranceFeeBy(double increment) {
		return entranceFee + increment;
	}
	
	public double getEntranceFeeJava() {
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		Application application = facesContext.getApplication();
		ExpressionFactory expressionFactory = application.getExpressionFactory();
		ELContext elContext = facesContext.getELContext();
		Double value = (Double)expressionFactory.createValueExpression(elContext, 
				"#{editSightView.entranceFee +100}", Double.class).getValue(elContext);
		return value;
	}
	
	public double incrementAndDecrementEntranceFeeBy(double increment, double decrement) {
		return entranceFee + increment - decrement;
	}
	
	public long getSightId() {
		return sightId;
	}
	
	public void setSightId(long sightId) {
		this.sightId = sightId;
	}
	
	public String getSightName() {
		return sightName;
	}
	
	public void setSightName(String sightName) {
		this.sightName = sightName;
	}
	
	public double getEntranceFee() {
		return entranceFee;
	}
	
	public void setEntranceFee(double entranceFee) {
		this.entranceFee = entranceFee;
	}
	
	public City getCity() {
		return city;
	}
	
	public void setCity(City city) {
		this.city = city;
	}
	
	public String[] getArray() {
		return array;
	}

	public void setArray(String[] array) {
		this.array = array;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}
}
