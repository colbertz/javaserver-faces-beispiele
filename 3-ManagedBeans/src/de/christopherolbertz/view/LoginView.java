package de.christopherolbertz.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

@ManagedBean
public class LoginView implements Serializable {
	private static final long serialVersionUID = -8237338749340885002L;
	private String username;
	private String password;
	private boolean rendered;

	public void click(ActionEvent actionEvent) {
		rendered = true;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean getRendered() {
		return rendered;
	}
}
