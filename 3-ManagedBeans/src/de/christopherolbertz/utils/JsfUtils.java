package de.christopherolbertz.utils;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;

public class JsfUtils {
	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	public static ExpressionFactory getExpressionFactory(final FacesContext facesContext) {
		return getApplication(facesContext).getExpressionFactory();
	}

	public static Application getApplication(final FacesContext facesContext) {
		final Application application = facesContext.getApplication();
		return application;
	}

	public static ELContext getELContext() {
		return FacesContext.getCurrentInstance().getELContext();
	}
	
	public static Object getBeanAttribute(String attributeName, String beanName, Class returnType ) {
		FacesContext facesContext = getFacesContext();
		ExpressionFactory expressionFactory = getExpressionFactory(facesContext);
		ELContext elContext = getELContext();
		String elExpression = "#{" + beanName + "." + attributeName + "}";
		
		return expressionFactory.createValueExpression(elContext, elExpression, returnType).getValue(elContext);
	}
	
	public static Double getDoubleBeanAttribute(String attributeName, String beanName) {
		Object result = getBeanAttribute(attributeName, beanName, Double.class);
		return (Double)result;
	}
}
